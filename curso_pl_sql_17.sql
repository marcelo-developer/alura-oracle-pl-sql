select * from  cliente;

declare
    v_segmercado_id cliente.segmercado%type := 1;
    v_contador number(3) := 1;
begin
    loop
        atualizar_cli_segmercado(v_contador, v_segmercado_id);
        v_contador := v_contador + 1;
    exit when
        v_contador > 6;
    end loop;
end;
        