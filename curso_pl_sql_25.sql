create or replace procedure atualizar_cli_segmercado
(p_id cliente.id%type, p_segmercado_id cliente.segmercado%type)
is
    e_cliente_id_inexistente exception;
begin
    update cliente set segmercado = p_segmercado_id where id = p_id;
    
    if sql%notfound then
        raise e_cliente_id_inexistente;
    end if;
    
    commit;
exception
    when e_cliente_id_inexistente then
        raise_application_error(-20040, 'Cliente n�o existe!!!');
end;

execute atualizar_cli_segmercado(12, 2);