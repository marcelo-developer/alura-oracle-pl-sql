declare
    cursor cur_cliente is select id from cliente;
    v_segmercado_id cliente.segmercado%type := 1;
begin
    for cli_rec in cur_cliente loop
       atualizar_cli_segmercado(p_segmercado_id => v_segmercado_id, p_id => cli_rec.id);
    end loop;
end;


select * from cliente;