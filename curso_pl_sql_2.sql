insert into segmercado (id, descricao) values (1, 'Varejo');

select * from segmercado;

delete from segmercado;

declare
    v_id segmercado.id%type := 2;
    v_descricao segmercado.descricao%type := 'Atacado';
begin
    insert into segmercado (id, descricao) values (v_id, v_descricao);
    commit;
end;