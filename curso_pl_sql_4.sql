declare
    v_id segmercado.id%type := 2;
    v_descricao segmercado.descricao%type := 'Atacadista';
begin
    update segmercado set descricao = upper(v_descricao) where id = v_id;
    v_id := 1;
    v_descricao := 'Varejista';
    update segmercado set descricao = upper(v_descricao) where id = v_id;
    commit;
end;