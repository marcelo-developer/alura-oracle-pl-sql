create or replace function categoria(p_faturamento in cliente.faturamentoprevisto%type)
return
    cliente.categoria%type;
is
    v_faturamento_previsto cliente.faturamentoprevisto%type := 12000,
    v_categoria cliente.categoria%type;
begin
    if v_faturamento_previsto < 10000 then
        v_categoria := 'PEQUENO';
    elsif v_faturamento_previsto < 50000 then
        v_categoria := 'M�DIO';
    elsif v_faturamento_previsto < 100000 then
        v_categoria := 'M�DIO GRANDE';
    else v_faturamento_previsto then
        v_categoria := 'GRANDE';
    end if;
end;