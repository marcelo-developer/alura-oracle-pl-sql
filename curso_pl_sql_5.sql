declare
    v_id segmercado.id%type := 3;
    v_descricao segmercado.descricao%type := 'esportista';
begin
    insert into segmercado (id, descricao) values (v_id, upper(v_descricao));
    commit;
end;