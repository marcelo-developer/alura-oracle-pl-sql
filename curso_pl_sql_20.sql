declare
    cursor cur_cliente is select id from cliente;
    v_id cliente.id%type;
    v_segmercado_id cliente.segmercado%type := 2;
begin
    open cur_cliente;
    loop
        fetch cur_cliente into v_id;
    exit when cur_cliente%notfound;
       atualizar_cli_segmercado(p_segmercado_id => v_segmercado_id, p_id => v_id);
    end loop;
    close cur_cliente;
end;

select * from cliente;