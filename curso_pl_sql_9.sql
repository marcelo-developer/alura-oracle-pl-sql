set serveroutput on

declare
    v_id segmercado.id%type := 2;
    v_descricao segmercado.descricao%type;
begin
    select descricao into v_descricao from segmercado where id = v_id;
    dbms_output.put_line(v_descricao);
end;