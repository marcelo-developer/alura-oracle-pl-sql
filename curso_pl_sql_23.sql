select * from cliente;

execute incluir_cliente(10, 'Mercadinho', '65487', 1, 200000);

execute incluir_cliente(null, 'Raise error', '65487', 1, 200000);

create or replace procedure incluir_cliente (
    p_id in cliente.id%type,
    p_razaosocial in cliente.razaosocial%type,
    p_cnpj in cliente.cnpj%type,
    p_segmercado in cliente.segmercado%type, 
    p_faturamentoprevisto in cliente.faturamentoprevisto%type
)
is
    v_categoria cliente.categoria%type;
    v_cnpj cliente.cnpj%type := p_cnpj;
    e_null exception;
    pragma exception_init(e_null, -1400);
begin
    formata_cnpj(v_cnpj);
    v_categoria := categoria(p_faturamentoprevisto);
    insert into cliente(id, razaosocial, cnpj, segmercado, datainclusao, faturamentoprevisto, categoria)
    values (
        p_id,
        p_razaosocial,
        v_cnpj,
        p_segmercado,
        sysdate,
        p_faturamentoprevisto,
        v_categoria);
    commit;
exception
    when dup_val_on_index then
        raise_application_error(-20010, 'Cliente j� cadastrado!!!!!!!!!!');
    when e_null then
        raise_application_error(-20015, 'A coluna id n�o pode receber valores nulos ou vazios!!!!!!!!!!');
end;