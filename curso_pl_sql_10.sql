create or replace function obter_descricao_segmercado(
    p_id in segmercado.id%type
)
return
    segmercado.descricao%type
is
    v_descricao segmercado.descricao%type;
begin
    select descricao into v_descricao from segmercado where id = p_id;
    return v_descricao;
end;
