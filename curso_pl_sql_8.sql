create or replace procedure incluir_segmercado
(p_id in segmercado.id%type, p_descricao in segmercado.descricao%type)
is
begin
    insert into segmercado (id, descricao) values (p_id, upper(p_descricao));
    commit;
end;