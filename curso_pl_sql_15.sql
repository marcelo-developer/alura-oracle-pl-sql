select * from cliente;

select substr(cnpj, 1, 3) || '/' || substr(cnpj, 4, 2) from cliente;

create or replace procedure formata_cnpj (
    p_cnpj
) is
begin
    p_cnpj := substr(cnpj, 1, 3) || '/' || substr(cnpj, 4, 2);
end;
