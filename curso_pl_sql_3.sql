declare
    v_id segmercado.id%type := 2;
    v_descricao segmercado.descricao%type := 'Atacado';
begin
    insert into segmercado (id, descricao) values (v_id, upper(v_descricao));
    commit;
end;

delete from segmercado where id = 2;