select * from cliente;

declare
    v_segmercado_id cliente.segmercado%type := 4;
begin
    for v_contador in 1..6 loop
        atualizar_cli_segmercado(p_segmercado_id => v_segmercado_id, p_id => v_contador);
    end loop;
end;