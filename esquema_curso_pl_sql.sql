create table SegMercado
(id number(5), descricao varchar2(100));

create table Cliente
(id number(5), RazaoSocial varchar2(100), cnpj varchar2(20), SegMercado number(5), DataInclusao date, FaturamentoPrevisto number(10,2), Categoria varchar2(20));

alter table segmercado add constraint segmercado_id_pk primary key (id);

alter table cliente add constraint cliente_id_pk primary key (id);

alter table cliente add constraint cliente_segmarcado_fk foreign key (segmercado) REFERENCES segmercado(id);