variable g_descricao varchar2(100)

execute :g_descricao := obter_descricao_segmercado(1)

print g_descricao;


set serveroutput on

declare
    v_descricao segmercado.descricao%type;
begin
    v_descricao := obter_descricao_segmercado(2);
    dbms_output.put_line('A descri��o do segmento de mercado � ' || v_descricao);
end;