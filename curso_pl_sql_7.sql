create procedure incluir_segmercado
(p_id in number, p_descricao in varchar2)
is
begin
    insert into segmercado (id, descricao) values (p_id, upper(p_descricao));
    commit;
end;


execute incluir_segmercado(3, 'farmaceuticos');

select * from segmercado;

begin
    incluir_segmercado(4, 'industrial');
end;