select * from cliente;

execute incluir_cliente(9, 'Mercadinho S�o Jo�o', '65476', 1, 120000);

set serveroutput on;

declare
    cursor cur_cliente is select id, razaosocial from cliente;
    v_id cliente.id%type;
    v_razaosocial cliente.razaosocial%type;
begin
    open cur_cliente;
    loop
        fetch cur_cliente into v_id, v_razaosocial;
    exit when cur_cliente%notfound;
        dbms_output.put_line('ID = ' || v_id);
        dbms_output.put_line('RAZAOSOCIAL = ' || v_razaosocial);
    end loop;
    close cur_cliente;
end;