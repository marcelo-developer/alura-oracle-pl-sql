create or replace procedure incluir_cliente (
    p_id cliente.id%type,
    p_razaosocial cliente.razaosocial%type,
    p_cpnj cliente.cpnj%type,
    p_segmercado cliente.segmercado%type, 
    p_datainclusao cliente.datainclusao%type, 
    p_faturamentoprevisto cliente.faturamentoprevisto%type
)
is
begin
    insert into cliente(id, razaosocial, cnpj, segmercado, datainclusao, faturamentoprevisto, categoria)
    values (
        p_id,
        p_razaosocial,
        p_cpnj,
        p_segmercado,
        p_datainclusao,
        p_faturamentoprevisto);
    commit;
end;