create or replace procedure incluir_cliente (
    p_id in cliente.id%type,
    p_razaosocial in cliente.razaosocial%type,
    p_cnpj in cliente.cnpj%type,
    p_segmercado in cliente.segmercado%type, 
    p_faturamentoprevisto in cliente.faturamentoprevisto%type
)
is
    v_categoria cliente.categoria%type;
begin
    v_categoria := categoria(p_faturamentoprevisto);
    insert into cliente(id, razaosocial, cnpj, segmercado, datainclusao, faturamentoprevisto, categoria)
    values (
        p_id,
        p_razaosocial,
        p_cnpj,
        p_segmercado,
        sysdate,
        p_faturamentoprevisto,
        v_categoria);
    commit;
end;


select * from cliente;

delete from cliente;

execute incluir_cliente(2, 'teste', '67654', 1, 90000);